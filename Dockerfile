FROM gps-registry:5000/gps-base:latest
RUN mkdir -p /opt/expedia/config-client

WORKDIR /opt/expedia/config-client

COPY target/*.jar /opt/expedia/config-client/config-client.jar

EXPOSE 81

CMD ["java" , "-jar", "-Dspring.boot.admin.url=${ADMIN_SERVER}", "-Dspring.boot.admin.client.serviceUrl=${APP_URL}", "/opt/expedia/config-client/config-client.jar"]
 
